import { PayloadAction, configureStore, createSlice } from '@reduxjs/toolkit';
import { FlightDetail } from './types';

interface TravelRequestState {
  departure: string;
  destination: string;
  travelDate: string;
  notes: string;
  selectedFlight?: FlightDetail;
}

const initialState: TravelRequestState = {
  departure: '',
  destination: '',
  travelDate: new Date().toLocaleDateString('en-US', {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  }),
  notes: '',
  selectedFlight: undefined,
};

const travelReqSlice = createSlice({
  name: 'travel_request',
  initialState,
  reducers: {
    updateDeparture: (state, action: PayloadAction<string>) => {
      state.departure = action.payload;
    },
    updateDestination: (state, action: PayloadAction<string>) => {
      state.destination = action.payload;
    },
    swapLocations: state => {
      const departure = state.departure;
      state.departure = state.destination;
      state.destination = departure;
    },
    updateTravelDate: (state, action: PayloadAction<string>) => {
      state.travelDate = action.payload;
    },
    selectFlight: (state, action: PayloadAction<FlightDetail>) => {
      state.selectedFlight = action.payload;
    },
    updateNotes: (state, action: PayloadAction<string>) => {
      state.notes = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  updateDeparture,
  updateDestination,
  swapLocations,
  updateTravelDate,
  updateNotes,
  selectFlight,
} = travelReqSlice.actions;

const store = configureStore({
  reducer: {
    travel_request: travelReqSlice.reducer,
  },
});

// Infer the `RootState` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

export default store;
