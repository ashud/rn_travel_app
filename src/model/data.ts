export const ACCORDIAN_LIST = [
  {
    title: 'Smart recommendation',
    message:
      'Our system will recommend the best flight according to your criteria and filters',
  },
  {
    title: 'Realtime Ticket Price',
    message:
      "The prices of flight tickets are updated in realtime, so you're always up to date with latest information",
  },
  {
    title: 'Smart Roundtrip',
    message: 'Easily book round trips and generally with better discounts',
  },
  {
    title: 'Smart Refund',
    message:
      'If you bought your ticket or related travel products and services through NgeFly, you can request a refund or check your refund status.',
  },
];

export const SOCIALS = [
  {
    iconName: 'logo-github',
    color: 'black',
    url: 'https://github.com/Aashu-Dubey',
  },
  {
    iconName: 'logo-twitter',
    color: '#1d9bf0',
    url: 'https://twitter.com/aashudubey_Ad',
  },
  {
    iconName: 'logo-youtube',
    color: '#FF0000',
    url: 'https://youtube.com/@massive.educator',
  },
  {
    iconName: 'logo-instagram',
    color: '#FF0169',
    url: 'https://instagram.com/massive.educator',
  },
  {
    iconName: 'logo-facebook',
    color: '#0062E0',
    url: 'https://facebook.com/massive.educator',
  },
];
