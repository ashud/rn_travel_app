import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export const WELCOME_MOBILE = 'welcome_mobile';
export const WELCOME_DESKTOP = 'welcome_desktop';
export const TRAVEL_REQUEST = 'travel_request';
export const FLIGHTS_LIST = 'flights_list';

export type RootStackParamList = {
  welcome_desktop: undefined;
  welcome_mobile: undefined;
  travel_request: undefined;
  flights_list: undefined;
};

// Welcome Desktop
export type WelcomeDeskNavProp = NativeStackNavigationProp<
  RootStackParamList,
  'welcome_desktop'
>;

// Welcome Scene
export type WelcomMobNavProp = NativeStackNavigationProp<
  RootStackParamList,
  'welcome_mobile'
>;

// Travel Request
export type TravelReqNavProp = NativeStackNavigationProp<
  RootStackParamList,
  'travel_request'
>;

// Flights List
export type FlightsListNavProp = NativeStackNavigationProp<
  RootStackParamList,
  'flights_list'
>;
