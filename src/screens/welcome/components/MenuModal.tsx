import React, { useEffect, useRef } from 'react';
import { Animated, SafeAreaView, StyleSheet, View } from 'react-native';
import Button from '../../components/Button';
import GetIcon from '../../components/GetIcon';
import { icon, isSfOrMaterial } from '../../../util/icon';

interface MenuProps {
  toggleMenu: boolean;
  onClose: () => void;
}

const GROUP_BUTTONS = ['Features', 'Discover', 'How it works', 'Resources'];

const MenuModal: React.FC<MenuProps> = ({ toggleMenu, onClose }) => {
  const menuAnim = useRef(new Animated.Value(0));
  const authBtnAnim = useRef(new Animated.Value(0));

  useEffect(() => {
    Animated.timing(menuAnim.current, {
      toValue: toggleMenu ? 1 : 0,
      duration: 500,
      useNativeDriver: false,
    }).start();

    Animated.timing(authBtnAnim.current, {
      toValue: toggleMenu ? 1 : 0,
      duration: toggleMenu ? 300 : 0,
      delay: toggleMenu ? 500 : 0,
      useNativeDriver: false,
    }).start();
  }, [toggleMenu]);

  const bgHeight = menuAnim.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });
  const btnConatinerOpacity = menuAnim.current.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });
  const menuTransform = menuAnim.current.interpolate({
    inputRange: [0, 1],
    outputRange: [-20, 0],
    extrapolate: 'clamp',
  });

  return (
    <View
      style={{ ...StyleSheet.absoluteFillObject }}
      pointerEvents={toggleMenu ? 'auto' : 'none'}
    >
      <Animated.View
        style={{ backgroundColor: 'rgb(23, 27, 34)', height: bgHeight }}
      />
      <SafeAreaView style={{ flex: 1, ...StyleSheet.absoluteFillObject }}>
        <Animated.View
          style={{ flexGrow: 1, padding: 8, opacity: btnConatinerOpacity }}
        >
          <Button
            style={{ alignSelf: 'flex-end' }}
            textStyle={{ fontSize: 20, fontWeight: '700', color: 'white' }}
            icon={
              <GetIcon
                name={icon.close}
                color="rgb(82, 176, 167)"
                size={32}
                type={isSfOrMaterial}
              />
            }
            onPress={onClose}
          />

          <Animated.View style={{ transform: [{ translateY: menuTransform }] }}>
            {GROUP_BUTTONS.map(button => (
              <Button
                key={button}
                style={styles.groupBtn}
                textStyle={styles.groupBtnTitle}
                title={button}
                color="white"
              />
            ))}
          </Animated.View>
        </Animated.View>
        <Animated.View
          style={[styles.authBtnContainer, { opacity: authBtnAnim.current }]}
        >
          <Button style={[styles.authBtn, styles.btnOutline]} title="Login" />
          <Button style={styles.authBtn} title="Sign up" isFill />
        </Animated.View>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  groupBtn: {
    justifyContent: 'flex-start',
    paddingVertical: 16,
  },
  groupBtnTitle: {
    fontSize: 20,
    fontWeight: '700',
  },
  authBtnContainer: {
    flexDirection: 'row',
    gap: 16,
    padding: 16,
  },
  authBtn: {
    flex: 1,
    paddingVertical: 12,
  },
  btnOutline: {
    borderWidth: 0.5,
    borderColor: 'rgb(82, 176, 167)',
  },
});

export default MenuModal;
