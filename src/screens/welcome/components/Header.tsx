import React, { useCallback, useEffect, useRef, useState } from 'react';
import { StyleSheet, View, useWindowDimensions } from 'react-native';
import Button from '../../components/Button';
import GetIcon from '../../components/GetIcon';
import BrandLogo from '../../components/BrandLogo';
import { icon, isSfOrMaterial } from '../../../util/icon';

interface Props {
  screenWidth: number;
  onMenuToggle?: (open: boolean) => void;
}

const Header: React.FC<Props> = ({ screenWidth, onMenuToggle }) => {
  const window = useWindowDimensions();

  const [isOverlapping, setOverlapping] = useState(false);
  const isOverlap = useRef(isOverlapping);

  const btnPos = useRef({ resources: 0, loginBtn: 0 });
  const resRef = useRef<View>(null);
  const loginBtnRef = useRef<View>(null);

  const showSwitchHeaderView = useCallback(() => {
    if (btnPos.current.loginBtn > 0 && btnPos.current.resources > 0) {
      const loginOverlapRes =
        btnPos.current.loginBtn <= btnPos.current.resources;
      const noOverlap = btnPos.current.loginBtn > btnPos.current.resources;

      if (
        (!isOverlap.current && loginOverlapRes) ||
        (isOverlap.current && noOverlap)
      ) {
        isOverlap.current = loginOverlapRes;
        setOverlapping(loginOverlapRes);
      }
    }
  }, []);

  useEffect(() => {
    resRef.current?.measure((_x, _y, _w, _h, pageX, _pY) => {
      btnPos.current.resources = pageX + _w;
    });
    loginBtnRef.current?.measure((_x, _y, _w, _h, pageX, _pY) => {
      btnPos.current.loginBtn = pageX;
    });

    showSwitchHeaderView();
  }, [window, showSwitchHeaderView]);

  // Fun Fact: this '!isOverlapping' condition makes no difference in result
  // TODO: figure this out.
  return (
    <View>
      {!isOverlapping || screenWidth >= 768 ? (
        <View style={{ flexDirection: 'row', padding: 24 }}>
          <BrandLogo />
          <View style={styles.buttonsContainer}>
            <Button title="Features" color="grey" />
            <Button title="Discover" color="grey" />
            <Button title="How it works" color="grey" />
            <View
              ref={resRef}
              onLayout={_e =>
                resRef.current?.measure((_x, _y, _w, _h, pageX, _pY) => {
                  btnPos.current.resources = pageX + _w;
                  showSwitchHeaderView();
                })
              }
            >
              <Button title="Resources" color="grey" />
            </View>
          </View>

          <View
            ref={loginBtnRef}
            onLayout={_e =>
              loginBtnRef.current?.measure((_x, _y, _w, _h, pageX, _pY) => {
                btnPos.current.loginBtn = pageX;
                showSwitchHeaderView();
              })
            }
          >
            <Button title="Login" />
          </View>
          <Button title="Sign up" isFill />
        </View>
      ) : (
        <View style={[styles.buttonsContainer, { paddingVertical: 8 }]}>
          <Button
            style={{ paddingHorizontal: 8, alignSelf: 'center' }}
            title="Sign up"
            isFill
          />
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <BrandLogo />
          </View>
          <Button
            style={{ paddingHorizontal: 8 }}
            icon={
              <GetIcon
                name={icon.menu}
                color="rgb(82, 176, 167)"
                size={32}
                type={isSfOrMaterial}
              />
            }
            onPress={() => onMenuToggle?.(true)}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
});

export default Header;
