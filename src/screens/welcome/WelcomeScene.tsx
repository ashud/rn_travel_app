import React from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/Button';
import BrandLogo from '../components/BrandLogo';
import { AppImages } from '../../assets';
import { WelcomMobNavProp } from '../../Routes';

const WelcomeScene = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation<WelcomMobNavProp>();

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.brandContainer}>
        <BrandLogo />
      </View>
      <View style={{ flex: 1 }}>
        <Image
          style={styles.mapBgImage}
          source={AppImages.dotted_world_map}
          resizeMode="contain"
        />
        <View style={styles.mapBgMask} />
      </View>
      <View
        style={[
          styles.contentConatainer,
          {
            paddingBottom: insets.bottom + 16,
            paddingLeft: insets.left + 16,
            paddingRight: insets.right + 16,
          },
        ]}
      >
        <View style={styles.titlesContainer}>
          <Text style={styles.title}>Discover your Dream Flight Easily</Text>
          <Text style={styles.subtitle}>
            find an easy way to buy airplane tickets with just a few clicks in
            the application.
          </Text>
        </View>

        <Button
          style={{ paddingVertical: 16, marginVertical: 24 }}
          textStyle={{ fontWeight: '700' }}
          title="Get Started"
          isFill
          onPress={() => navigation.navigate('travel_request')}
        />
        <Text style={styles.footerText}>
          Already have an account?{' '}
          <Text style={styles.loginTxtBtn} onPress={() => {}}>
            Login
          </Text>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(23, 27, 34)',
  },
  brandContainer: {
    alignItems: 'center',
  },
  brandName: {
    color: 'white',
    letterSpacing: 4,
    paddingVertical: 8,
  },
  brandNameStyled: {
    color: 'rgb(82, 176, 167)',
    fontStyle: 'italic',
  },
  mapBgImage: {
    width: '100%',
    height: 300,
    transform: [{ perspective: 1000 }, { rotateX: '40deg' }, { scale: 1.5 }],
  },
  mapBgMask: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(23, 27, 34, 0.6)',
    width: '100%',
    height: '100%',
    zIndex: 1000,
  },
  contentConatainer: {
    ...StyleSheet.absoluteFillObject,
    paddingTop: 32,
  },
  titlesContainer: { flexGrow: 1, justifyContent: 'center' },
  title: {
    maxWidth: 420,
    fontSize: 60,
    fontWeight: 'bold',
    color: 'white',
    lineHeight: 60,
  },
  subtitle: {
    fontSize: 16,
    color: 'rgba(255, 255, 255, 0.5)',
    paddingVertical: 16,
    lineHeight: 24,
    marginTop: 40,
  },
  footerText: {
    color: 'rgba(255, 255, 255, 0.5)',
    textAlign: 'center',
  },
  loginTxtBtn: { color: 'white', marginLeft: 4 },
});

export default WelcomeScene;
