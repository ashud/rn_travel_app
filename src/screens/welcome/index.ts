import WelcomeScene from './WelcomeScene';
import WelcomeSceneDesktop from './WelcomeScene.desktop';

export default { Mobile: WelcomeScene, Desktop: WelcomeSceneDesktop };
