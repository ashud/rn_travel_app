import { Platform, StyleSheet } from 'react-native';

export const fStyles = StyleSheet.create({
  infoTextCorner: {
    flex: 1,
    color: 'grey',
    fontSize: 12,
    fontWeight: 'bold',
  },
  centerText: {
    flex: 1,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 16,
  },
  middleRowTextCorner: {
    flexGrow: 0.5,
    fontSize: 24,
    fontWeight: 'bold',
  },
  originalPrice: {
    fontSize: 12,
    color: 'grey',
    textDecorationLine: 'line-through',
    marginLeft: 8,
  },
  viewCircleBorder: {
    width: 10,
    height: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgb(231, 236, 243)',
  },
  viewCircleBg: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'rgb(231, 236, 243)',
  },
  flightIconContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  flightIcon: {
    ...Platform.select({
      default: {
        textShadowColor: 'rgba(82, 176, 167, 0.6)',
        textShadowOffset: { width: 0, height: 4 },
        textShadowRadius: 10,
      },
      web: { textShadow: '0px 4px 10px rgba(82, 176, 167, 0.6)' },
    }),
  },
});
