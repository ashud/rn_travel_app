import React, { useCallback, useState } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import DateTimePicker, {
  DateTimePickerAndroid,
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import { Config } from '../../../config';

interface Props {
  value: string;
  onChange: (selectedDate?: Date | undefined) => void;
}

const DatePicker: React.FC<Props> = ({ value, onChange }) => {
  const [dateSelected, setDateSelected] = useState(new Date());

  const onDateChange = useCallback(
    (_e: DateTimePickerEvent, selectedDate?: Date | undefined) => {
      selectedDate && setDateSelected(selectedDate);
      onChange(selectedDate);
    },
    [onChange],
  );

  const onButtonPress = () =>
    Config.isAndroid &&
    DateTimePickerAndroid.open({
      testID: 'dateTimePicker',
      value: dateSelected,
      mode: 'date',
      minimumDate: new Date(),
      onChange: onDateChange,
    });

  return Config.isIos || Config.isWindows ? (
    <DateTimePicker
      testID="dateTimePicker"
      value={dateSelected}
      mode="date"
      minimumDate={new Date()}
      onChange={onDateChange}
      themeVariant="light"
    />
  ) : Config.isWeb ? (
    // Temporary solution for web
    <input
      type="date"
      id="start"
      name="trip-start"
      value={dateSelected.toISOString().split('T')[0]}
      min={new Date().toISOString().split('T')[0]}
      onChange={(event: any) =>
        event.target.value && onDateChange(event, new Date(event.target.value))
      }
    />
  ) : (
    <TouchableOpacity onPress={onButtonPress}>
      <Text style={{ fontSize: 16, fontWeight: '500' }} numberOfLines={2}>
        {value ?? 'Travel Date'}
      </Text>
    </TouchableOpacity>
  );
};

export default DatePicker;
