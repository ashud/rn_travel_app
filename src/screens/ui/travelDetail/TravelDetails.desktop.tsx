import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import Tabs from './Tabs';
import DatePicker from './DatePicker';
import Button from '../../components/Button';
import Switch from '../../components/Switch';
import GetIcon from '../../components/GetIcon';
import { Alert } from '../../components/Alert.web';
import {
  RootState,
  updateDeparture,
  updateDestination,
  updateTravelDate,
} from '../../../store';
import { Config } from '../../../config';
import * as Route from '../../../Routes';
import { icon, isSfOrIon, isSfOrMaterial } from '../../../util/icon';

const SearchInputSection = () => {
  const navigation = useNavigation<Route.TravelReqNavProp>();
  const travelState = useSelector((state: RootState) => state.travel_request);
  const dispatch = useDispatch();

  const onSearchFlights = () => {
    if (
      travelState.departure.trim().length === 0 ||
      travelState.destination.trim().length === 0 ||
      travelState.travelDate.trim().length === 0
    ) {
      Alert.alert('Please fill all the fields');
      return;
    }
    navigation.navigate('flights_list');
  };

  const onDateChange = (selectedDate?: Date | undefined) => {
    const date = selectedDate?.toLocaleDateString('en-US', {
      weekday: 'short',
      year: 'numeric',
      month: 'short',
      day: 'numeric',
    });
    // selectedDate && setDateSelected(selectedDate);
    date && dispatch(updateTravelDate(date));
  };

  return (
    <View style={styles.travelInputsContainer}>
      <Tabs isFullTabView />
      <ScrollView
        style={{ paddingVertical: 8 }}
        horizontal
        showsHorizontalScrollIndicator={false}
        alwaysBounceHorizontal={false}
      >
        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          <View style={styles.fieldBorderContainer}>
            <View style={styles.fieldContainer}>
              <View style={styles.iconContainer}>
                <GetIcon
                  name={icon.plane_takeoff}
                  color="white"
                  size={18}
                  type={isSfOrMaterial}
                />
              </View>
              <View
                style={[styles.verticalDivider, { marginHorizontal: 16 }]}
              />
              <View style={{ justifyContent: 'center' }}>
                <Text style={styles.fieldTitle}>Departure city</Text>
                <TextInput
                  style={styles.valueText}
                  placeholder="Enter City"
                  placeholderTextColor="lightgrey"
                  accessibilityLabel="Departure"
                  aria-label="Departure"
                  value={travelState.departure}
                  onChangeText={text => dispatch(updateDeparture(text))}
                />
              </View>
            </View>
            <View style={styles.verticalDivider} />
            <View style={styles.fieldContainer}>
              <View style={styles.iconContainer}>
                <GetIcon
                  name={icon.plane_land}
                  color="white"
                  size={18}
                  type={isSfOrMaterial}
                />
              </View>
              <View
                style={[styles.verticalDivider, { marginHorizontal: 16 }]}
              />
              <View style={{ justifyContent: 'center' }}>
                <Text style={styles.fieldTitle}>Arrival city</Text>
                <TextInput
                  style={styles.valueText}
                  placeholder="Enter City"
                  placeholderTextColor="lightgrey"
                  accessibilityLabel="Destination"
                  aria-label="Destination"
                  value={travelState.destination}
                  onChangeText={text => dispatch(updateDestination(text))}
                />
              </View>
            </View>
          </View>

          <View style={styles.fieldBorderContainer}>
            <View style={styles.fieldContainer}>
              <View style={styles.iconContainer}>
                <GetIcon
                  name={icon.calendar}
                  color="white"
                  size={18}
                  type={isSfOrIon}
                />
              </View>
              <View
                style={[styles.verticalDivider, { marginHorizontal: 16 }]}
              />
              <View style={{ justifyContent: 'center' }}>
                <Text style={styles.fieldTitle}>Date of departure</Text>
                <View style={Config.isIos && { left: -10 }}>
                  <DatePicker
                    value={travelState.travelDate}
                    onChange={onDateChange}
                  />
                </View>
              </View>
            </View>
            <View style={styles.verticalDivider} />
            <View style={styles.returnSwitchContainer}>
              <Text style={styles.fieldTitle}>Return ?</Text>
              <Switch />
            </View>
          </View>

          <View style={styles.fieldBorderContainer}>
            <View style={styles.fieldContainer}>
              <View style={styles.iconContainer}>
                <GetIcon
                  name={icon.person_circle}
                  color="white"
                  size={18}
                  type={isSfOrIon}
                />
              </View>
              <View
                style={[styles.verticalDivider, { marginHorizontal: 16 }]}
              />

              <View style={{ justifyContent: 'center' }}>
                <Text style={styles.fieldTitle}>Passengers</Text>
                <TouchableOpacity>
                  <Text style={styles.valueText}>2 Seat</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.verticalDivider} />
            <View style={{ justifyContent: 'center', padding: 16 }}>
              <Text style={styles.fieldTitle}>Class</Text>
              <TouchableOpacity>
                <Text style={styles.valueText}>Business</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Button
          style={styles.searchBtn}
          title="Search"
          isFill
          icon={
            <GetIcon
              name={icon.search}
              color="white"
              size={24}
              type={isSfOrIon}
            />
          }
          accessibilityLabel="searchButton"
          aria-label="searchButton"
          accessibilityRole="button"
          onPress={onSearchFlights}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  travelInputsContainer: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgb(231, 236, 243)',
    borderRadius: 16,
    padding: 16,
    paddingVertical: 8,
    backgroundColor: 'white',
    shadowColor: 'lightgrey',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 8,
  },
  fieldBorderContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: 'rgb(231, 236, 243)',
    borderRadius: 8,
    margin: 8,
  },
  fieldContainer: {
    flexDirection: 'row',
    padding: 16,
  },
  iconContainer: {
    width: 34,
    height: 34,
    backgroundColor: 'rgb(82, 176, 167)',
    padding: 8,
    borderRadius: 16,
    alignSelf: 'center',
    // boxShadow: '0px 8px 10px rgba(82, 176, 167, 0.6)',
    shadowColor: 'rgb(82, 176, 167)',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 8,
  },
  verticalDivider: {
    width: 1,
    backgroundColor: 'rgb(231, 236, 243)',
  },
  fieldTitle: {
    fontSize: 12,
    fontWeight: '600',
    color: 'grey',
    marginBottom: 8,
  },
  valueText: {
    fontSize: 16,
    fontWeight: '500',
  },
  returnSwitchContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  searchBtn: {
    flexDirection: 'column',
    margin: 8,
    paddingHorizontal: 8,
  },
});

export default SearchInputSection;
