import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Tabs from './Tabs';
import DatePicker from './DatePicker';
import Switch from '../../components/Switch';
import Button from '../../components/Button';
import GetIcon from '../../components/GetIcon';
import { Alert } from '../../components/Alert.web';
import {
  RootState,
  swapLocations,
  updateDeparture,
  updateDestination,
  updateTravelDate,
} from '../../../store';
import * as Route from '../../../Routes';
import { icon, isSfOrIon, isSfOrMaterial } from '../../../util/icon';
import { Platform } from 'react-native';

interface Props {
  isFullTabView?: boolean;
}

const TravelDetails: React.FC<Props> = ({ isFullTabView }) => {
  const navigation = useNavigation<Route.TravelReqNavProp>();
  const travelState = useSelector((state: RootState) => state.travel_request);
  const dispatch = useDispatch();

  const onSearchFlights = () => {
    if (
      travelState.departure.trim().length === 0 ||
      travelState.destination.trim().length === 0 ||
      travelState.travelDate.trim().length === 0
    ) {
      Alert.alert('Please fill all the fields');
      return;
    }
    navigation.navigate('flights_list');
  };

  const onDateChange = (selectedDate?: Date | undefined) => {
    const date = selectedDate?.toLocaleDateString('en-US', {
      weekday: 'short',
      year: 'numeric',
      month: 'short',
      day: 'numeric',
    });
    // selectedDate && setDateSelected(selectedDate);
    date && dispatch(updateTravelDate(date));
  };

  return (
    <View style={styles.travelInfoContainer}>
      <Tabs {...{ isFullTabView }} />

      <View style={styles.fieldContainer}>
        <View style={{ flexGrow: 1 }}>
          <View style={{ flexDirection: 'row' }}>
            <GetIcon
              style={styles.inputIcon}
              name={icon.plane_takeoff}
              color="grey"
              size={28}
              type={isSfOrMaterial}
            />
            <View style={styles.verticalDivider} />
            <TextInput
              style={styles.inputField}
              placeholder="Departure city"
              placeholderTextColor="lightgrey"
              accessibilityLabel="Departure"
              aria-label="Departure"
              value={travelState.departure}
              onChangeText={text => dispatch(updateDeparture(text))}
            />
          </View>
          <View style={styles.horizontalDivider} />
          <View style={{ flexDirection: 'row' }}>
            <GetIcon
              style={styles.inputIcon}
              name={icon.plane_land}
              color="grey"
              size={28}
              type={isSfOrMaterial}
            />
            <View style={styles.verticalDivider} />
            <TextInput
              style={styles.inputField}
              placeholder="Arrival city"
              placeholderTextColor="lightgrey"
              accessibilityLabel="Destination"
              aria-label="Destination"
              value={travelState.destination}
              onChangeText={text => dispatch(updateDestination(text))}
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.swapLocation}
          accessibilityLabel="swapBtn"
          aria-label="swapBtn"
          accessibilityRole="button"
          role="button"
          onPress={() => dispatch(swapLocations())}
        >
          <GetIcon
            style={styles.swapLocationIcon}
            name="swap-vert"
            color="white"
            size={32}
          />
        </TouchableOpacity>
      </View>

      <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
        <View style={[{ flexGrow: 1 }, styles.fieldContainer]}>
          <GetIcon
            style={styles.inputIcon}
            name={icon.calendar}
            color="grey"
            size={28}
            type={isSfOrIon}
          />
          <View style={styles.verticalDivider} />

          <View style={{ justifyContent: 'center' }}>
            <DatePicker
              value={travelState.travelDate}
              onChange={onDateChange}
            />
          </View>
        </View>
        <View style={styles.returnSwitchContainer}>
          <Text style={styles.returnText}>Return ?</Text>
          <Switch />
        </View>
      </View>
      <View style={styles.businessSeatContainer}>
        <View style={[{ flexGrow: 2 }, styles.fieldContainer]}>
          <GetIcon
            style={styles.inputIcon}
            name={icon.person_circle}
            color="grey"
            size={28}
            type={isSfOrIon}
          />
          <View style={styles.verticalDivider} />
          <TouchableOpacity style={{ alignSelf: 'center', padding: 8 }}>
            <Text style={{ fontSize: 16, fontWeight: '500' }}>Business</Text>
          </TouchableOpacity>
        </View>
        <View style={[{ flexGrow: 1 }, styles.fieldContainer]}>
          <GetIcon
            style={styles.inputIcon}
            name={icon.person_circle}
            color="grey"
            size={28}
            type={isSfOrIon}
          />
          <View style={styles.verticalDivider} />
          <TouchableOpacity style={{ alignSelf: 'center', padding: 8 }}>
            <Text style={{ fontSize: 16, fontWeight: '500' }}>2 Seat</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Button
        style={styles.searchButton}
        textStyle={styles.searchText}
        title="Search"
        isFill
        accessibilityLabel="searchButton"
        aria-label="searchButton"
        accessibilityRole="button"
        role="button"
        onPress={onSearchFlights}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  travelInfoContainer: {
    backgroundColor: 'white',
    borderRadius: 16,
    padding: 16,
    paddingHorizontal: 24,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgb(231, 236, 243)',
    shadowColor: 'lightgrey',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 8,
  },
  inputIcon: {
    alignSelf: 'center',
    marginHorizontal: 4,
  },
  verticalDivider: {
    width: 1,
    backgroundColor: 'rgb(231, 236, 243)',
    marginHorizontal: 8,
    marginVertical: 4,
  },
  inputField: {
    flex: 1,
    padding: 8,
    fontSize: 16,
    fontWeight: '500',
  },
  horizontalDivider: {
    height: 1,
    backgroundColor: 'rgb(231, 236, 243)',
    marginVertical: 8,
  },
  swapLocation: {
    backgroundColor: 'rgb(82, 176, 167)',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 16,
    marginRight: 4,
    shadowColor: 'grey',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.2,
    shadowRadius: 7.65,
    elevation: 8,
  },
  swapLocationIcon: {
    position: 'absolute',
    ...Platform.select({
      default: {
        textShadowColor: 'white',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 16,
      },
      web: { textShadow: '0px 0px 16px white' },
    }),
  },
  fieldContainer: {
    flexDirection: 'row',
    flexShrink: 1,
    overflow: 'scroll',
    borderWidth: 1,
    borderColor: 'rgb(231, 236, 243)',
    marginTop: 16,
    padding: 12,
    borderRadius: 8,
  },
  searchButton: {
    paddingVertical: 16,
    marginTop: 24,
    marginBottom: 8,
    shadowColor: 'rgb(82, 176, 167)',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 8,
  },
  searchText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
  },
  returnSwitchContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 16,
    marginTop: 16,
    rowGap: 4,
  },
  returnText: {
    color: 'grey',
    fontSize: 12,
    fontWeight: '600',
  },
  businessSeatContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    columnGap: 16,
  },
});

export default TravelDetails;
