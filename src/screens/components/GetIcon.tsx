import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { StyleProp, TextStyle } from 'react-native';
import { Config } from '../../config';

let SFSymbol: typeof React.Component;

if (Config.isIos) {
  var { SFSymbol: SFIcon } = require('react-native-sfsymbols');
  SFSymbol = SFIcon;
}

interface Props {
  name: string;
  color: string;
  size: number;
  type?: 'material' | 'ion' | 'sf';
  style?: StyleProp<TextStyle>;
}

const GetIcon: React.FC<Props> = ({
  name,
  color,
  size,
  type = 'material',
  style,
}) => {
  const IconType = type === 'material' ? Icon : Ionicons;
  const isSfAndIos = Config.isIos && type === 'sf';

  return !isSfAndIos ? (
    <IconType {...{ style, name, color, size }} />
  ) : (
    <SFSymbol
      style={[{ width: size, height: size }, style]}
      {...{ name, color, size }}
      scale="small"
    />
  );
};

export default GetIcon;
