import React, { useEffect, useRef } from 'react';
import {
  Animated,
  Modal,
  Platform,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import GetIcon from './GetIcon';
import { FilterValues } from '../../types';
import { Config } from '../../config';

interface ModalProps {
  showFilter: boolean;
  filterValues: FilterValues;
  setShowFilter: React.Dispatch<React.SetStateAction<boolean>>;
  setFilterValues: React.Dispatch<React.SetStateAction<FilterValues>>;
  onSubmit: () => void;
}

interface CheckProps {
  title: string;
  isChecked: boolean;
  onCheckChange: () => void;
}

const CheckBox: React.FC<CheckProps> = ({
  title,
  isChecked,
  onCheckChange,
}) => {
  return (
    <Pressable style={styles.airlineCheck} onPress={onCheckChange}>
      <GetIcon
        name={isChecked ? 'check-box' : 'check-box-outline-blank'}
        color={isChecked ? '#54D3C2' : 'lightgrey'}
        size={25}
      />
      <Text style={{ color: 'black', marginStart: 4 }}>{title}</Text>
    </Pressable>
  );
};

const FilterUI: React.FC<ModalProps> = ({
  filterValues,
  setShowFilter,
  setFilterValues,
  onSubmit,
}) => {
  const renderSortIcon = (type: 'asc' | 'desc') => {
    const isSelected = filterValues.priceValues.sort === type;

    return (
      <Pressable
        style={({ pressed }) => [
          {
            backgroundColor: isSelected ? '#54D3C2' : 'rgb(245, 247, 250)',
            padding: 4,
            borderRadius: 4,
            opacity: pressed ? 0.6 : 1,
          },
        ]}
        onPress={() =>
          setFilterValues(vals => {
            vals.priceValues.sort = vals.priceValues.sort !== type ? type : '';
            return { ...vals };
          })
        }
      >
        <GetIcon
          name={`arrow-${type === 'asc' ? 'downward' : 'upward'}`}
          color={isSelected ? 'white' : 'black'}
          size={20}
        />
      </Pressable>
    );
  };

  return (
    <TouchableWithoutFeedback
      style={{ flex: 1 }}
      onPress={() => setShowFilter(false)}
    >
      <SafeAreaView style={styles.filterContainer}>
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={() => {}}>
          <View style={styles.filterResponsiveContainer}>
            <View style={styles.filterInnerContainer}>
              <Text style={styles.filterTitle}>Filter</Text>

              <Text style={styles.filterSectionTitle}>Price Range</Text>

              <View style={{ flexDirection: 'row', marginVertical: 8 }}>
                <View style={styles.filterPriceContainer}>
                  <Text style={{ fontSize: 16 }}>₹</Text>
                  <TextInput
                    style={{ flex: 1, fontSize: 16, marginStart: 8 }}
                    placeholder="start"
                    keyboardType="number-pad"
                    value={`${filterValues.priceValues.min}`}
                    onChangeText={min =>
                      // setPriceValues({ ...priceValues, min: Number(min) })
                      setFilterValues(vals => {
                        vals.priceValues.min = Number(min);
                        return { ...vals };
                      })
                    }
                  />
                </View>
                <View style={{ width: 50 }} />
                <View style={styles.filterPriceContainer}>
                  <Text style={{ fontSize: 16 }}>₹</Text>
                  <TextInput
                    style={{ flex: 1, fontSize: 16, marginStart: 8 }}
                    placeholder="End"
                    keyboardType="number-pad"
                    value={`${filterValues.priceValues.max}`}
                    onChangeText={max =>
                      // setPriceValues({ ...priceValues, max: Number(max) })
                      setFilterValues(vals => {
                        vals.priceValues.max = Number(max);
                        return { ...vals };
                      })
                    }
                  />
                </View>
              </View>
              <View
                style={{ flexDirection: 'row', paddingVertical: 8, gap: 8 }}
              >
                {renderSortIcon('asc')}
                {renderSortIcon('desc')}
              </View>
              <Text style={styles.filterSectionTitle}>Airlines</Text>
              {filterValues.airlines.map(air => (
                <CheckBox
                  key={air.name}
                  title={air.name}
                  isChecked={air.isSelected}
                  onCheckChange={() => {
                    air.isSelected = !air.isSelected;
                    // setAirlines([...airlines]);
                    setFilterValues({
                      ...filterValues,
                      airlines: [...filterValues.airlines],
                    });
                  }}
                />
              ))}
              <Pressable
                style={({ pressed }) => [
                  styles.applyFilterBtn,
                  { opacity: pressed ? 0.6 : 1 },
                ]}
                onPress={onSubmit}
              >
                <Text style={styles.applyText}>Apply</Text>
              </Pressable>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const FilterModal: React.FC<ModalProps> = props => {
  const [visible, setVisible] = React.useState(props.showFilter);
  const fadeAnim = useRef(new Animated.Value(props.showFilter ? 1 : 0));

  useEffect(() => {
    Animated.timing(fadeAnim.current, {
      toValue: props.showFilter ? 1 : 0,
      duration: 400,
      useNativeDriver: true,
    }).start(() => setVisible(props.showFilter));
  }, [props.showFilter]);

  return Config.isDesktop ? (
    <Animated.View
      style={{ ...StyleSheet.absoluteFillObject, opacity: fadeAnim.current }}
      pointerEvents={visible ? 'auto' : 'none'}
    >
      <FilterUI {...props} />
    </Animated.View>
  ) : (
    <Modal
      visible={props.showFilter}
      animationType="slide"
      transparent
      onRequestClose={() => props.setShowFilter(false)}
    >
      <FilterUI {...props} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  filterContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    justifyContent: 'center',
  },
  filterResponsiveContainer: {
    width: '100%',
    maxWidth: 600,
    alignSelf: 'center',
  },
  filterInnerContainer: {
    backgroundColor: 'white',
    margin: 24,
    padding: 16,
    borderRadius: 16,
  },
  filterTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 8,
  },
  filterSectionTitle: {
    fontSize: 18,
    color: 'grey',
    fontWeight: 'bold',
    marginVertical: 8,
  },
  filterPriceContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    borderBottomWidth: 0.5,
    borderBottomColor: 'darkgrey',
  },
  airlineCheck: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 8,
  },
  applyFilterBtn: {
    backgroundColor: 'rgb(82, 176, 167)',
    borderRadius: 8,
    padding: 16,
    marginTop: 24,
    marginBottom: 8,
    elevation: 8,
    ...Platform.select({
      default: {
        shadowColor: 'rgb(82, 176, 167)',
        shadowOffset: { width: 0, height: 8 },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
      },
      web: { boxShadow: '0px 8px 10.32px rgba(82, 176, 167, 0.44)' },
    }),
  },
  applyText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
  },
});

export default FilterModal;
