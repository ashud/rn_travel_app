import React, { useState } from 'react';
import {
  Pressable,
  PressableProps,
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  ViewStyle,
} from 'react-native';

interface ButtonProps extends PressableProps {
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  title?: string;
  isFill?: boolean;
  color?: string;
  icon?: any;
}

const Button: React.FC<ButtonProps> = ({
  style,
  textStyle,
  title,
  isFill,
  color,
  icon,
  ...props
}) => {
  const [hovered, setHovered] = useState(false);

  return (
    <Pressable
      {...props}
      style={({ pressed }) => [
        styles.container,
        {
          backgroundColor: isFill
            ? hovered
              ? 'rgba(82, 176, 167, 0.8)'
              : 'rgb(82, 176, 167)'
            : undefined,
        },
        style,
        hovered && { opacity: 0.8 },
        pressed && { opacity: 0.6 },
      ]}
      onHoverIn={() => setHovered(true)}
      onHoverOut={() => setHovered(false)}
    >
      {icon}
      <Text
        style={[
          styles.title,
          { color: !isFill ? color || 'rgb(82, 176, 167)' : 'white' },
          textStyle,
          { display: title ? 'flex' : 'none' },
        ]}
      >
        {title}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    padding: 16,
    paddingVertical: 8,
  },
  title: {
    textAlign: 'center',
    marginHorizontal: 8,
  },
});

export default Button;
