import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import GetIcon from './GetIcon';

const BrandLogo = () => {
  return (
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <GetIcon
        // @ts-expect-error: Incompatible properties
        style={{
          marginEnd: 2,
          ...Platform.select({
            default: { transform: [{ rotate: '45deg' }] },
            web: { transform: 'rotate(45deg)' },
          }),
        }}
        name="flight"
        color="rgb(82, 176, 167)"
        size={18}
      />
      <Text style={styles.brandName}>
        NGE
        <Text style={styles.brandNameStyled}>FLY</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  brandName: {
    color: 'white',
    letterSpacing: 4,
    paddingVertical: 8,
  },
  brandNameStyled: {
    color: 'rgb(82, 176, 167)',
    fontStyle: 'italic',
  },
});

export default BrandLogo;
