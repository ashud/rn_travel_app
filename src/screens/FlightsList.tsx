import React, { useEffect, useRef, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Pressable,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import FlightItem from './ui/flightItem/FlightItem';
import FlightItemDesktop from './ui/flightItem/FlightItem.desktop';
import FilterModal from './components/FilterModal';
import GetIcon from './components/GetIcon';
import { RootState, selectFlight } from '../store';
import { AppImages } from '../assets';
import { FilterValues, FlightDetail } from '../types';
import * as Route from '../Routes';
import { icon, isSfOrMaterial } from '../util/icon';
import { Config } from '../config';

const FILTER_INITIAL: FilterValues = {
  airlines: [],
  priceValues: { min: 0, max: 0, sort: '' },
};

// mainly to reset filter values, as it directly modifies the object values,
// which affect both state and reference, so for now using this solution
const deepClone = (object: Object) => JSON.parse(JSON.stringify(object));

const FlightsList = () => {
  const navigation = useNavigation<Route.FlightsListNavProp>();
  const insets = useSafeAreaInsets();
  const travelState = useSelector((state: RootState) => state.travel_request);
  const dispatch = useDispatch();

  const [screenDims, setScreenDims] = useState({ width: 0, height: 0 });
  const [showFilter, setShowFilter] = useState<boolean>(false);
  const [flights, setFlights] = useState<FlightDetail[]>([]);
  const [filterResult, setFilterResult] = useState<FlightDetail[] | null>(null);
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL);
  const filterValuesInitial = useRef(deepClone(FILTER_INITIAL));

  useEffect(() => {
    const fetchFlights = () => {
      fetch('https://api.npoint.io/99590c838ef0ac108dbe', { method: 'GET' })
        .then(response => response.json())
        .then((res: { data: { result: FlightDetail[] } }) => {
          setFlights(res.data.result);

          const airList: FilterValues['airlines'] = [];
          let max = 0;
          const filterArr = res.data.result.filter((item, index, self) => {
            if (item.fare > max) {
              max = item.fare;
            }
            return (
              index ===
              self.findIndex(
                t =>
                  t.displayData.airlines[0].airlineCode ===
                  item.displayData.airlines[0].airlineCode,
              )
            );
          });

          filterArr.forEach(air =>
            airList.push({
              name: air.displayData.airlines[0].airlineName,
              isSelected: true,
            }),
          );

          /* res.data.result.forEach(flight => {
            const flightAirlines = flight.displayData.airlines;

            flightAirlines.forEach(air => {
              !airList.includes(air.airlineName) &&
                airList.push(air.airlineName);
            });
          }); */

          setFilterValues(fVals => {
            const newValues = {
              airlines: airList,
              priceValues: { ...fVals.priceValues, max },
            };
            filterValuesInitial.current = deepClone(newValues);

            return newValues;
          });
        })
        .catch(err => console.log('flight request failed', err));
    };

    fetchFlights();
  }, []);

  const onFilterFlight = () => {
    const { airlines, priceValues } = filterValues;
    const enabledAirlines: string[] = [];
    airlines.forEach(air => air.isSelected && enabledAirlines.push(air.name));

    const filterRes = flights.filter(
      flight =>
        flight.fare >= priceValues.min &&
        flight.fare <= priceValues.max &&
        enabledAirlines.includes(flight.displayData.airlines[0].airlineName),
    );

    if (priceValues.sort !== '') {
      const isAsc = priceValues.sort === 'asc';
      filterRes.sort((val1, val2) =>
        isAsc ? val1.fare - val2.fare : val2.fare - val1.fare,
      );
    }

    setFilterResult(filterRes);
    setShowFilter(false);
  };

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="rgb(23, 27, 34)" />
      <View
        style={{ flex: 1, backgroundColor: 'rgb(245, 247, 250)' }}
        onLayout={event => {
          const { width, height } = event.nativeEvent.layout;
          setScreenDims({ width, height });
        }}
      >
        <View style={[styles.header, { overflow: 'hidden' }]}>
          <View style={{ ...StyleSheet.absoluteFillObject }}>
            <Image
              style={[styles.mapBg, { height: screenDims.height / 3 }]}
              source={AppImages.dotted_world_map}
              resizeMode="contain"
            />
          </View>
          <View style={[styles.mapBgMask, { height: screenDims.height / 3 }]} />
          <SafeAreaView style={[styles.headerInner, { zIndex: 2 }]}>
            <TouchableOpacity
              style={[styles.headerBtn, { paddingLeft: Config.isIos ? 8 : 16 }]}
              activeOpacity={0.6}
              onPress={() => navigation.goBack()}
            >
              <GetIcon
                name={icon.arrow_back}
                color="lightgrey"
                size={24}
                type={isSfOrMaterial}
              />
            </TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.headerLocatonTxt}>
                  {travelState.departure}
                </Text>
                <GetIcon
                  style={{ marginHorizontal: 8 }}
                  name={icon.arrow_forward}
                  color="lightgrey"
                  size={24}
                  type={isSfOrMaterial}
                />
                <Text style={styles.headerLocatonTxt}>
                  {travelState.destination}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 8,
                }}
              >
                <Text style={{ color: 'darkgrey', fontSize: 12 }}>
                  {travelState.travelDate}
                </Text>
                <Text style={{ color: 'darkgrey', marginHorizontal: 8 }}>
                  •
                </Text>
                <Text style={{ color: 'darkgrey', fontSize: 12 }}>2 Seat</Text>
                <Text style={{ color: 'darkgrey', marginHorizontal: 8 }}>
                  •
                </Text>
                <Text style={{ color: 'darkgrey', fontSize: 12 }}>
                  Business
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.headerBtn}
              activeOpacity={0.6}
              onPress={() => setShowFilter(true)}
            >
              <GetIcon name="filter-alt" color="lightgrey" size={24} />
            </TouchableOpacity>
          </SafeAreaView>
        </View>
        <SafeAreaView style={{ flex: 1 }}>
          <View style={[styles.sectionRow, { padding: 16 }]}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
              Search Result
            </Text>
            <Text style={{ fontSize: 12, color: 'grey', fontWeight: 'bold' }}>
              "{filterResult ? filterResult.length : flights.length} Results"
            </Text>
          </View>
          <View
            style={[
              styles.sectionRow,
              { paddingHorizontal: 16, marginBottom: 8 },
            ]}
          >
            <View style={styles.filterInfoChip}>
              <Text style={{ color: 'grey', fontWeight: 'bold' }}>
                ₹{filterValues.priceValues.min} - ₹
                {filterValues.priceValues.max}
              </Text>
            </View>
            <Pressable
              style={[
                styles.clearFilterBtn,
                { display: filterResult ? 'flex' : 'none' },
              ]}
              onPress={() => {
                setFilterResult(null);
                setFilterValues(deepClone(filterValuesInitial.current));
              }}
            >
              <GetIcon
                name={icon.close}
                color="lightgrey"
                size={14}
                type={isSfOrMaterial}
              />
              <Text style={{ color: 'white', fontSize: 12 }}>Clear</Text>
            </Pressable>
          </View>

          <FlatList
            contentContainerStyle={[
              styles.listContentContainer,
              { paddingBottom: insets.bottom + 16 },
            ]}
            /* key={screenDims.width > 1200 ? 3 : screenDims.width > 768 ? 2 : 1}
          numColumns={screenDims.width > 1200 ? 3 : screenDims.width > 768 ? 2 : 1} */
            data={filterResult === null ? flights : filterResult}
            renderItem={({ item }) =>
              screenDims.width <= 768 ? (
                <FlightItem
                  info={item}
                  onClick={() => {
                    dispatch(selectFlight(item));
                    navigation.goBack();
                  }}
                />
              ) : (
                <FlightItemDesktop
                  info={item}
                  onClick={() => {
                    dispatch(selectFlight(item));
                    navigation.goBack();
                  }}
                />
              )
            }
            keyExtractor={(item, index) => `${item.id}_${index}`}
            ListEmptyComponent={
              filterResult === null ? (
                <ActivityIndicator style={{ flex: 1 }} size="large" />
              ) : (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={styles.noResultFound}>No result found</Text>
                </View>
              )
            }
          />

          {/* <View
          style={{
            position: 'absolute',
            bottom: 32,
            left: 0,
            right: 0,
            // backgroundColor: 'red',
            alignItems: 'center',
            shadowColor: 'grey',
            shadowOffset: { width: 0, height: 4 },
            shadowOpacity: 0.3,
            shadowRadius: 4.65,
            elevation: 8,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              borderRadius: 32,
              backgroundColor: 'white',
            }}
          >
            <Pressable
              style={({ pressed }) => [
                {
                  padding: 16,
                  flexDirection: 'row',
                  alignItems: 'center',
                  opacity: pressed ? 0.6 : 1,
                },
              ]}
            >
              <Icon name="filter-list" color="lightgrey" size={28} />
              <Text style={{ fontWeight: '500', paddingHorizontal: 16 }}>
                Sort
              </Text>
            </Pressable>
            <View
              style={{
                width: 0.5,
                backgroundColor: 'lightgrey',
                marginVertical: 8,
              }}
            />
            <Pressable
              style={({ pressed }) => [
                {
                  padding: 16,
                  flexDirection: 'row',
                  alignItems: 'center',
                  opacity: pressed ? 0.6 : 1,
                },
              ]}
            >
              <Icon name="filter-alt" color="lightgrey" size={28} />
              <Text style={{ fontWeight: '500', paddingHorizontal: 16 }}>
                Filter
              </Text>
            </Pressable>
          </View>
        </View> */}
        </SafeAreaView>
      </View>

      <FilterModal
        {...{ showFilter, filterValues, setShowFilter, setFilterValues }}
        onSubmit={onFilterFlight}
      />
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: 'rgb(23, 27, 34)',
    padding: 16,
    paddingVertical: 24,
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
  },
  mapBg: {
    width: '100%',
    maxWidth: 600,
    alignSelf: 'center',
    transform: [{ rotateX: '30deg' }, { scale: 1.2 }],
  },
  mapBgMask: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(23, 27, 34, 0.6)',
    width: '100%',
    zIndex: 1,
  },
  headerInner: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerBtn: {
    height: 46,
    width: 46,
    backgroundColor: 'rgba(128, 128, 128, 0.4)',
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  headerLocatonTxt: {
    color: 'white',
    fontSize: 18,
    fontWeight: '600',
  },
  sectionRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  filterInfoChip: {
    backgroundColor: 'rgba(128, 128, 128, 0.2)',
    borderRadius: 8,
    padding: 8,
    paddingVertical: 4,
  },
  clearFilterBtn: {
    flexDirection: 'row',
    backgroundColor: 'rgb(23, 27, 34)',
    borderRadius: 16,
    padding: 8,
    paddingVertical: 4,
    alignItems: 'center',
  },
  listContentContainer: {
    flexGrow: 1,
    padding: 8,
  },
  noResultFound: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default FlightsList;
