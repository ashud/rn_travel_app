// API data
export interface FlightDetail {
  id: string;
  fare: number;
  originalFare: number;
  filterFlag?: string;
  displayData: {
    source: {
      airport: {
        cityCode: string;
        cityName: string;
        terminal: string;
        airportCode: string;
        airportName: string;
        countryCode: string;
        countryName: string;
      };
      depTime: string;
    };
    airlines: {
      airlineCode: string;
      airlineName: string;
      flightNumber: string;
      logo: string;
    }[];
    stopInfo: string;
    destination: {
      airport: {
        cityCode: string;
        cityName: string;
        terminal: string;
        airportCode: string;
        airportName: string;
        countryCode: string;
        countryName: string;
      };
      arrTime: string;
    };
    totalDuration: string;
  };
}

// Flight Filter Modal
export type FilterValues = {
  airlines: {
    name: string;
    isSelected: boolean;
  }[];
  priceValues: {
    min: number;
    max: number;
    sort: '' | 'asc' | 'desc';
  };
};
