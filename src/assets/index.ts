const AppImages = {
  dotted_world_map: require('./dotted_world_map.jpeg'),
  userImage: require('./userImage.png'),
  business_icon: require('./business_icon.png'),

  // Web
  playStoreBadge: require('./welcome/google-play-badge.png'),
  appStoreBadge: require('./welcome/app-store-badge.png'),
  flight_icon: require('./welcome/flight_icon.png'),
  map_icon: require('./welcome/map_icon.png'),
  down_arrow: require('./welcome/down_arrow.png'),
  travel_req_half_frame: require('./welcome/travel_req_half_frame.png'),
  travel_req_form: require('./welcome/travel_req_form.png'),
  flight_item_card: require('./welcome/flight_item_card.png'),
  ngefly_preview: require('./welcome/ngefly_preview.png'),
  // Airlines
  airlines: {
    singapore: require('./welcome/airlines/singapore_airlines.png'),
    delta: require('./welcome/airlines/delta_airlines.png'),
    british: require('./welcome/airlines/british_airways.png'),
    american: require('./welcome/airlines/american_airlines.png'),
  },
};

export { AppImages };
