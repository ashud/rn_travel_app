export const formatTime = (date: string) => {
  const d = new Date(date);
  const timeConvention = d.getHours() > 12 ? 'PM' : 'AM';

  const hours = String(d.getHours()).padStart(2, '0');
  return `${hours}:${d.getMinutes()} ${timeConvention}`;
};
