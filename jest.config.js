module.exports = {
  preset: 'react-native',
  setupFilesAfterEnv: ['./jest-setup.js'],
  transformIgnorePatterns: [
    'node_modules/(?!(jest-)?@?react-native|@react-native-community|@react-navigation|react-redux)',
  ],
  // make 'test-utils' available without relative imports (../)
  moduleDirectories: ['node_modules', '__tests__/utils'],
  // ignore files in utils directory for testing
  modulePathIgnorePatterns: ['<rootDir>/__tests__/utils'],
};
